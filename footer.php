<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package kah-hero
 */
?>

	</div><!-- #content -->
</div><!-- #page -->
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div id="foot-constraint">
			<div class="footer-menu clear">
<!-- https://codex.wordpress.org/Function_Reference/has_nav_menu -->
 <?php
if ( has_nav_menu( 'footer' ) ) {
     wp_nav_menu( array( 'theme_location' => 'footer' ) );
} ?> 
			</div>
			<div class="left-side">


			<?php if ( ! dynamic_sidebar( 'sidebar-2' ) ) : ?>
			<?php endif; // end sidebar widget area ?>

			</div>

			<div class="right-side">

			<?php if(get_option('pdog_address') && get_option('pdog_address') != '') {?>
			<span class="icon-text"><?php echo get_option('pdog_address') ?></span> <?php } ?>

			<?php if(get_option('pdog_address2') && get_option('pdog_address2') != '') {?>
			<span class="icon-text"><?php echo get_option('pdog_address2') ?></span>  <?php } ?>

			<?php if(get_option('pdog_phone') && get_option('pdog_phone') != '') {?>
			<span class="icon-text"><?php echo get_option('pdog_phone') ?><?php } ?>	

			<?php if(get_option('pdog_phone2') && get_option('pdog_phone2') != '') {?>
			<span class="icon-text"><?php echo get_option('pdog_phone2') ?><?php } ?>						

			<span class="icon-text">&copy;<?php $the_year = date("Y"); echo $the_year; ?> <?php bloginfo( 'name' ); ?>, All rights reserved.
				<?php if(get_option('pdog_target') && get_option('pdog_target') != '') {?>
					Site design by <a href="<?php echo get_option('pdog_target') ?>" target="_blank"><?php echo get_option('pdog_attribute') ?></a>
				<?php } ?>	
			</span>

			
			</div>
			<div class="site-info">
			</div><!-- .site-info -->
		</div>
	</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>
