<?php
/*
*Template Name: Full Width
 * @package kah-hero
 */

get_header(); ?>


		<div id="page" class="hfeed site">
			<div id="header-bump"></div>

<?php if ( get_field( 'page_hero_image' ) ): ?>
<div id="hero-top">		
<img src="<?php echo esc_url( get_field( 'page_hero_image' ) ); ?>"/>	
<h1><?php the_title(); ?></h1>
</div>
<?php endif; ?>		
	<div id="content" class="site-content" >
	<div id="primary" class="full-content-area">
		<main id="main" class="full-site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
	<?php if ( !get_field( 'page_hero_image' ) ): ?>
	<h1><?php the_title(); ?></h1>	
	<?php endif; ?>	
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'kah-hero' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	<?php edit_post_link( __( 'Edit', 'kah-hero' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer>' ); ?>
</article><!-- #post-## -->



			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->
	<div class="clear" style="height:2em;"></div>

<?php get_footer(); ?>
